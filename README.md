# EvalKG

Evaluting the quality of a KG without gold standard via link prediction task.

## Setup

The experiment mainly used the datasets and link prediction models implemented in [Ampligraph 1.x](https://github.com/Accenture/AmpliGraph/tree/ampligraph1/develop), which depends on Python 3.7 and Tensorflow 1.15.x

It is recommended to manage the environment by Anaconda:

```
conda create -n eval-kg python=3.7
``` 
Then install the dependencies by pip 

```
conda activate eval-kg
pip install -r requirements.txt
```

## Run

After setting up the environment, running the experiment is simply running the scripts.

### Incorrect KG  

```
python incorrect.py -i ./data-input -o ./incorrect-output
```

After running this command, the `data-input` and `incorrect-output` will be created automatically. `data-input` will be used to cache the datasets downloaded by Ampligraph, and the experiment reports will be written to `incorrect-output`.

### Incomplete KG

```
python incomplete.py -i ./data-input -o ./incomplete-output
```

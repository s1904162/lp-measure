
import os
import random
from copy import deepcopy
from datetime import datetime
from typing import Iterable

import numpy as np
import pandas as pd
import tensorflow as tf
from ampligraph.utils import save_model
from ampligraph.evaluation import (create_mappings, to_idx, 
                                   generate_corruptions_for_fit,
                                   train_test_split_no_unseen,
                                   evaluate_performance,
                                   mrr_score, hits_at_n_score)


def get_cls_name(obj):
    '''Get the name of the class of the object
    
    Example
    -------
    >>> l = [1, 2, 3]
    >>> get_cls_name(l)
    'list'
    '''
    return obj.__class__.__name__


def numerise_kg(train, valid, test):
    trps = np.concatenate([train, valid, test])
    rel_to_idx, ent_to_idx = create_mappings(trps)
    id_train = to_idx(train, ent_to_idx, rel_to_idx).astype(np.int32)
    id_valid = to_idx(valid, ent_to_idx, rel_to_idx).astype(np.int32)
    id_test = to_idx(test, ent_to_idx, rel_to_idx).astype(np.int32)
    return id_train, id_valid, id_test

def sample_negative(id_trps):
    assert id_trps.dtype == np.int32, f'dtype of input triples should be int32, not {id_trps.dtype}'
    neg = generate_corruptions_for_fit(id_trps).eval(session=tf.Session())
    return neg

def inject_incorrect(id_train, proportion=0.3) -> np.ndarray:
    sampled = random.sample(id_train.tolist(), int(len(id_train)*proportion))
    generated_incorrect = sample_negative(np.array(sampled, dtype=np.int32))
    # replace the original sampled triples with incorrect ones
    original = tuple(map(tuple, id_train))
    sampled = tuple(map(tuple, sampled))
    remaind = list(set(original) - set(sampled))
    return np.vstack([remaind, generated_incorrect])

def make_incomplete(id_train, proportion=0.3) -> np.ndarray:
    sampled = random.sample(id_train.tolist(), int(len(id_train)*proportion))
    # generated_incorrect = sample_negative(np.array(sampled, dtype=np.int32))
    # replace the original sampled triples with incorrect ones
    original = tuple(map(tuple, id_train))
    sampled = tuple(map(tuple, sampled))
    remaind = list(set(original) - set(sampled))
    return np.array(remaind)

def measure_kg(lpmodel, kg:np.ndarray, beta=0.1):
    '''
    Measure the quality of a KG via Link Prediction
    '''
    G, g = train_test_split_no_unseen(kg, test_size=int(len(kg)*beta))
    lpmodel.fit(G)
    ranks = evaluate_performance(g, model=lpmodel, filter_triples=kg)
    return {
        'mrr': mrr_score(ranks),
        'hit@1': hits_at_n_score(ranks, 1),
        'hit@3': hits_at_n_score(ranks, 5),
        'hit@10': hits_at_n_score(ranks, 10)
    }
    
def experiment(lpmodel, kg, rates=[0.001, 0.25, 0.50], kgname='kg', expname=None, output_dir='./', exptype='incorrect'):

    if exptype == 'incorrect':
        neg_kgs = [inject_incorrect(kg, rate) for rate in rates]
    elif exptype == 'incomplete':
        neg_kgs = [make_incomplete(kg, rate) for rate in rates]
    else:
        raise ValueError(f'Invalid experiment type: {exptype}')
    
    models = [deepcopy(lpmodel) for _ in range(len(neg_kgs))]

    if expname == None:
        expname = datetime.now().strftime('%m.%d-%H.%M.%S')
    exppath = os.path.join(output_dir, expname)

    res = pd.DataFrame({
        f'{exptype} {rate}': measure_kg(mdl, kg) 
            for rate, kg, mdl in zip(rates, neg_kgs, models)
    }).round(3)

    print(get_cls_name(lpmodel), kgname)
    print(res.to_markdown())
    if not os.path.exists(exppath):
        os.mkdir(exppath)
    with open(os.path.join(exppath,'report.txt'), 'a') as f:
        f.write(kgname+'\n============\n')
        f.write(res.to_latex()+'\n\n')
    try:
        for rate, kg, mdl in zip(rates, neg_kgs, models):
            save_model(mdl, os.path.join(exppath, f'{get_cls_name(mdl)}-{kgname}_{rate}.pt'))
    except TypeError as err:
        print(err)
    return 
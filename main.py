import argparse
import os

import numpy as np
from ampligraph.latent_features import (
    TransE, 
    ComplEx, 
    ConvE, 
    ConvKB
)
from ampligraph.datasets import (
    load_fb15k, 
    load_fb15k_237, 
    load_wn18, 
    load_wn18rr
)

from lp_measure import numerise_kg, experiment

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
if not os.environ.get('AMPLIGRAPH_DATA_HOME'):
    os.environ['AMPLIGRAPH_DATA_HOME'] = os.path.join(
        ROOT_DIR, 'ampligraph_datasets'
    )


parser = argparse.ArgumentParser()

parser.add_argument('-i', '--input_dir')
parser.add_argument('-o', '--output_dir')
parser.add_argument('-e', '--experiment_type', default='incorrect')
args = parser.parse_args()
output_dir = args.output_dir
os.environ['AMPLIGRAPH_DATA_HOME'] = args.input_dir

print(os.environ['AMPLIGRAPH_DATA_HOME'])


hyperparams = {
        'verbose': True,
        'optimizer_params': {'lr': 1e-4},
        'epochs': 1000,
    }

transe = TransE(**hyperparams)
comp = ComplEx(**hyperparams)

fb15k237 = load_fb15k_237()
wn18rr = load_wn18rr()
fb15k = load_fb15k()
wn18 = load_wn18()

fb15k['name'] = 'fb15k'
wn18['name'] = 'wn18'
fb15k237['name'] = 'fb15k237'
wn18rr['name'] = 'wn18rr'


for ds in [fb15k, fb15k237, wn18, wn18rr]:
    id_train, id_valid, id_test = numerise_kg(ds['train'], ds['valid'], ds['test'])
    kg = np.vstack([id_train, id_test])

    experiment(comp, kg, kgname=ds['name'], expname='ComplEx', output_dir=output_dir, exptype=args.experiment_type)
    experiment(transe, kg, kgname=ds['name'], expname='TransE', output_dir=output_dir, exptype=args.experiment_type)
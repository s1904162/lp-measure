import rdflib

import numpy as np
from ampligraph.evaluation import train_test_split_no_unseen
from ampligraph.latent_features import (
    TransE, 
    ComplEx, 
)

g = rdflib.Graph()
g.parse('output/template/full_graph.rdf', format='xml')

trips = []
for s, p, o in g:
    trips.append([s, p, o])

trips = np.array(trips)
print(trips.shape)

id_train_valid, id_test = train_test_split_no_unseen(trips, test_size=0.2)
id_train, id_valid = train_test_split_no_unseen(id_train_valid, test_size=0.2)


ds = {
    'train': id_train,
    'valid': id_valid,
    'test': id_test,
}

hyperparams = {
        'verbose': True,
        'optimizer_params': {'lr': 1e-4},
        'epochs': 1000,
    }

transe = TransE(**hyperparams)
comp = ComplEx(**hyperparams)

from lp_measure import numerise_kg, experiment

id_train, id_valid, id_test = numerise_kg(ds['train'], ds['valid'], ds['test'])
kg = np.vstack([id_train, id_test])

experiment(comp, kg, kgname='synthetic', expname='ComplEx', output_dir='./output', exptype='incorrect')

experiment(comp, kg, kgname='synthetic', expname='ComplEx', output_dir='./output', exptype='incomplete')